## 2020
Visualisation cartographique réalisée pour une présentation de Sophie Vasset, qui a collecté les données dans le cadre de ses travaux pour l'obtention de l'HDR en 2020. [Voir la carte](https://vjurie.gitpages.huma-num.fr/gbwmap/)

## 2021-2023
Cette proposition a contribué au lancement d'un projet de recherche émergence *Res-sources : réseau documentaire et historique sur les eaux minérales – 1650-1850*, porté par Sophie Vasset et François Zanetti (Université Paris Cité). Un portail de consultation de données est en cours de construction avec le concours du centre des humanités numériques de l'Université Paris Cité (instance Heurist d'Huma-Num).

[Note](https://hackmd.io/@vjurie/bases_de_donnees_relationnelles) sur la base des bases de données relationnelles
